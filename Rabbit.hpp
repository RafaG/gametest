/* 
 * File:   Rabbit.hpp
 * Author: Mario
 *
 * Created on 19 de julio de 2015, 0:53
 */

#ifndef RABBIT_HPP
#define	RABBIT_HPP

#include "Actor.hpp"
#include "Animation.hpp"
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Audio.hpp>


class Rabbit : public Actor{
public:
    const float GROUND_HEIGHT;
    
    Rabbit();
    virtual ~Rabbit();
    void advanceTime(float deltaTime);
    void draw(sf::RenderTarget & target, sf::RenderStates states) const;
    void jump(float speed=750);
    sf::Vector2f getSize() const;
    
    int getLife() const;
    void setLife(int life);
    void setMaxLife(int maxLife);
    void increaseLife();
    void decreaseLife();
    bool isLifeFull() const;
    
    bool isDead();
    void reset();
    void startGodMode();
    bool isGod() const;
    bool isJumping() const;
    
    void shoot();
    bool isShooting() const;
    sf::Vector2f getBulletOutPosition() const;
    
private:
    static const float GRAVITY;
    bool jumping;
    Animation representation;
    int life, maxLife;
    sf::SoundBuffer bJump, bShoot;
    sf::Sound soundJump, soundShoot;
    
    sf::Clock godModeClock, godModeBlink;
    bool godModeOn;
    
    bool shooting;
    sf::Clock shootingClock;
    int lastFrameBeforeShooting;
    sf::Sprite shotgun;
    
    
};

#endif	/* RABBIT_HPP */

