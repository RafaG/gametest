/* 
 * File:   ParticleEngine.hpp
 * Author: Rafa
 *
 * Created on 18 de agosto de 2015, 23:35
 */

#ifndef PARTICLEENGINE_HPP
#define	PARTICLEENGINE_HPP

#include "../ActorManager.hpp"
#include "Particle.hpp"
#include <memory>

class ParticleGenerator;

class ParticleEngine : public ActorManager<Particle> {
public:
    ParticleEngine();
    ParticleEngine(const ParticleEngine& orig);
    virtual ~ParticleEngine();
    
    void advanceTime(float deltaTime);
    void addGenerator(ParticleGenerator *particleGenerator);
    
protected:
    bool isRemovable(Particle *actor) const;
private:
    std::vector<std::unique_ptr<ParticleGenerator>> generators;
};

#endif	/* PARTICLEENGINE_HPP */

