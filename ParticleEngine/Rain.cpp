/* 
 * File:   Rain.cpp
 * Author: Rafa
 * 
 * Created on 22 de agosto de 2015, 21:04
 */

#include "Rain.hpp"
#include "Drop.hpp"
#include "ParticleEngine.hpp"
#include <iostream>

Rain::Rain(ParticleEngine & particleEngine) : ParticleGenerator(particleEngine){
}

Rain::~Rain() {
}

void Rain::advanceTime(float deltaTime) {
    if (clock.getElapsedTime().asMilliseconds()>=10) {
        clock.restart();
        
        Particle * particle = new Drop(Drop::BLOOD);
        particle->setPosition(sf::Vector2f(rand()%800,-2));
        particle->setSpeed(sf::Vector2f(engine->getSpeed().x,300));
        engine->addActor(particle);
        
    }
}

bool Rain::hasFinished() const {
    return false;
}