/* 
 * File:   ParticleGenerator.hpp
 * Author: Rafa
 *
 * Created on 22 de agosto de 2015, 16:04
 */

#ifndef PARTICLEGENERATOR_HPP
#define	PARTICLEGENERATOR_HPP

#include <ESE/Core/Animatable.hpp>
class ParticleEngine;

class ParticleGenerator : ESE::Animatable{
public:
    ParticleGenerator(ParticleEngine & particleEngine);
    virtual ~ParticleGenerator();
    virtual void advanceTime(float deltaTime) = 0;
    virtual bool hasFinished() const = 0;
    
protected:
    ParticleEngine * engine;
};

#endif	/* PARTICLEGENERATOR_HPP */

