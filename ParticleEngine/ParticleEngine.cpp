/* 
 * File:   ParticleEngine.cpp
 * Author: Rafa
 * 
 * Created on 18 de agosto de 2015, 23:35
 */

#include "ParticleEngine.hpp"
#include "Particle.hpp"
#include "ParticleGenerator.hpp"
#include "../Config.hpp"
#include <iostream>

ParticleEngine::ParticleEngine() {
}

ParticleEngine::~ParticleEngine() {
    std::cout << "Particles left: " << actors.size() << std::endl;
    std::cout << "Generator left: " << generators.size() << std::endl;
}

bool ParticleEngine::isRemovable(Particle * actor) const {
    if (actor->getPosition().x > 800 || actor->getPosition().y > Config::HEIGHT) {
        return true;
    }
    if (actor->getPosition().x + actor->getSize().x < 0 || actor->getPosition().y + actor->getSize().y < 0) {
        return true;
    }
}

void ParticleEngine::advanceTime(float deltaTime) {
    for (auto it = generators.begin(); it!=generators.end();) {
        (*it)->advanceTime(deltaTime);
        if ((*it)->hasFinished()) {
            generators.erase(it);
        }
        else {
            ++it;
        }
    }
    
    for (auto it = actors.begin(); it!=actors.end();) {
        (*it)->advanceTime(deltaTime);
        
        if ((*it)->getPosition().y>580) {
            (*it)->setSpeed(getSpeed());
            (*it)->setState(Particle::LANDED);
        }
        
        if (isRemovable(*it)) {
            actors.erase(it);
        }
        else {
            ++it;
        }
    }
}

void ParticleEngine::addGenerator(ParticleGenerator* particleGenerator) {
    this->generators.push_back(std::unique_ptr<ParticleGenerator>(particleGenerator));
}