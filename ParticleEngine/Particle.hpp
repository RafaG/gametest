/* 
 * File:   Particle.hpp
 * Author: Rafa
 *
 * Created on 19 de agosto de 2015, 16:15
 */

#ifndef PARTICLE_HPP
#define	PARTICLE_HPP

#include "../Actor.hpp"

class Particle : public Actor {
public:
    
    enum State {FALLING, LANDED};
    
    Particle(const sf::Vector2f & size, const sf::Color & color);
    virtual ~Particle();

    void draw(sf::RenderTarget & target, sf::RenderStates states) const;
    sf::Vector2f getSize() const;
    void advanceTime(float deltaTime);
    
    State getState() const { return state; }
    virtual void setState(const State & state) { this->state = state; }
    
    virtual void setSize(const sf::Vector2f & size);
    
    
private:
    sf::RectangleShape representation;
    State state;
};

#endif	/* PARTICLE_HPP */

