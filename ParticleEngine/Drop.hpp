/* 
 * File:   Drop.hpp
 * Author: Rafa
 *
 * Created on 22 de agosto de 2015, 23:05
 */

#ifndef DROP_HPP
#define	DROP_HPP

#include "Particle.hpp"

class Drop : public Particle {
public:

    enum Type {
        WATER, BLOOD
    };
    Drop(const Type & type);
    virtual ~Drop();

    void setState(const State & state);

private:

};

#endif	/* DROP_HPP */

