/* 
 * File:   Drop.cpp
 * Author: Rafa
 * 
 * Created on 22 de agosto de 2015, 23:05
 */

#include "Drop.hpp"
#include "Particle.hpp"

Drop::Drop(const Type & type) : Particle(sf::Vector2f(1, 10), (type == BLOOD ? sf::Color::Red : sf::Color::Blue)) {

}

Drop::~Drop() {
}

void Drop::setState(const State& state) {
    Particle::setState(state);

    if (state == LANDED) {
        this->setSize(sf::Vector2f(4, 3));
    }
}
