/* 
 * File:   Particle.cpp
 * Author: Rafa
 * 
 * Created on 19 de agosto de 2015, 16:15
 */

#include "Particle.hpp"
#include <SFML/Graphics/RenderTarget.hpp>

Particle::Particle(const sf::Vector2f & size, const sf::Color & color) {
    representation.setSize(size);
    representation.setFillColor(color);
    state = FALLING;
}

Particle::~Particle() {
}

void Particle::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    states.transform*=this->getTransform();
    target.draw(representation,states);
}

sf::Vector2f Particle::getSize() const {
    return representation.getSize();
}

void Particle::advanceTime(float deltaTime) {
    move(getSpeed()*deltaTime);
    if (state==FALLING) {
        this->setSpeed(getSpeed()+sf::Vector2f(0,900*deltaTime));
    }
}

void Particle::setSize(const sf::Vector2f& size) {
    representation.setSize(size);
}