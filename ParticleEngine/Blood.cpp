/* 
 * File:   Blood.cpp
 * Author: Rafa
 * 
 * Created on 22 de agosto de 2015, 16:34
 */

#include "Blood.hpp"
#include <stdlib.h>
#include "Particle.hpp"
#include "ParticleEngine.hpp"
#include <iostream>

Blood::Blood(const sf::Vector2f & position, ParticleEngine & particleEngine) : ParticleGenerator(particleEngine){
    for (int i=400; i<=450; i+=50) {
        for (float angle = 2; angle<4.3; angle+=0.01) {
            
            float factorX = (1+rand()%20)/10.f;
            float factorY = (1+rand()%20)/10.f;
            
            Particle * particle = new Particle(sf::Vector2f(3,3),sf::Color::Red);
            particle->setPosition(position);
            particle->setSpeed(sf::Vector2f(cos(angle)*(i)*factorX,sin(angle)*300*factorY));
            engine->addActor(particle);
        }
    }
}

Blood::~Blood() {
}

void Blood::advanceTime(float deltaTime) {
    
}

bool Blood::hasFinished() const {
    return true;
}
