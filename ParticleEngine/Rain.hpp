/* 
 * File:   Rain.hpp
 * Author: Rafa
 *
 * Created on 22 de agosto de 2015, 21:04
 */

#ifndef RAIN_HPP
#define	RAIN_HPP

#include "ParticleGenerator.hpp"
#include <SFML/System/Clock.hpp>

class Rain : public ParticleGenerator{
public:
    Rain(ParticleEngine & particleEngine);
    virtual ~Rain();
    
    void advanceTime(float deltaTime);
    bool hasFinished() const;
    
private:
    sf::Clock clock;
};

#endif	/* RAIN_HPP */

