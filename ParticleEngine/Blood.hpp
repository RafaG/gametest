/* 
 * File:   Blood.hpp
 * Author: Rafa
 *
 * Created on 22 de agosto de 2015, 16:34
 */

#ifndef BLOOD_HPP
#define BLOOD_HPP

#include <SFML/System/Vector2.hpp>
#include "ParticleGenerator.hpp"

class Blood : public ParticleGenerator {
public:
    Blood(const sf::Vector2f & position, ParticleEngine & particleEngine);
    virtual ~Blood();
    virtual void advanceTime(float deltaTime);
    virtual bool hasFinished() const;
private:

};

#endif /* BLOOD_HPP */

