/* 
 * File:   GroundManager.hpp
 * Author: Rafa
 *
 * Created on 27 de julio de 2015, 17:30
 */

#ifndef GROUNDMANAGER_HPP
#define	GROUNDMANAGER_HPP

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <ESE/Core/Animatable.hpp>

class GroundManager : public sf::Drawable, public ESE::Animatable{
public:
    const float GROUND_HEIGHT;
    GroundManager();
    virtual ~GroundManager();
    
    void draw(sf::RenderTarget & target, sf::RenderStates states) const;
    void advanceTime(float deltaTime);
    
    void setSpeed(float speed);
    float getSpeed() const;
    
protected:
    void generateTerrain();
    int calcNextGeneration();
private:
    std::vector<sf::Sprite> terrain;
    sf::Sprite base;
    sf::Clock clock;
    int nextGeneration;
    float speed;
    
};

#endif	/* GROUNDMANAGER_HPP */

