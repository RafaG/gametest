/* 
 * File:   kamikazeBird.h
 * Author: Mario
 *
 * Created on 19 de junio de 2016, 16:45
 */

#ifndef KAMIKAZEBIRD_H
#define	KAMIKAZEBIRD_H

#include "Enemy.hpp"

class KamikazeBird : public Enemy{
public:
    KamikazeBird();
    void draw(sf::RenderTarget & target, sf::RenderStates states) const;
    virtual ~KamikazeBird();
    void setPosition(sf::Vector2f pos);
    sf::Vector2f getSize() const;
    void advanceTime(float deltaTime);
    bool acceptBulletDamage() const { return true; }

    
private:
    sf::Sprite representation;

};

#endif	/* KAMIKAZEBIRD_H */

