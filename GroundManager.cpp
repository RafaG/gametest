/* 
 * File:   GroundManager.cpp
 * Author: Rafa
 * 
 * Created on 27 de julio de 2015, 17:30
 */

#include "GroundManager.hpp"
#include <SFML/Graphics/RenderTarget.hpp>
#include <ESE/Core/Textures.hpp>
#include "Config.hpp"

GroundManager::GroundManager() :
GROUND_HEIGHT(25) {
    base.setTexture(*ESE::Textures::instance().getResource("GroundBase"));
    base.setPosition(0, Config::HEIGHT - base.getGlobalBounds().height);

    nextGeneration = calcNextGeneration();
    speed = 0.f;
}

GroundManager::~GroundManager() {
}

void GroundManager::draw(sf::RenderTarget & target, sf::RenderStates states) const {

    for (auto it = terrain.begin(); it != terrain.end(); ++it) {
        target.draw(*it, states);
    }

    target.draw(base, states);
}

void GroundManager::advanceTime(float deltaTime) {
    if (clock.getElapsedTime().asSeconds() > nextGeneration) {
        clock.restart();
        nextGeneration = calcNextGeneration();
        if (terrain.size() > 0) {
            if (terrain.back().getPosition().x + terrain.back().getGlobalBounds().width < Config::WIDTH) {
                generateTerrain();
            }
        } else {
            generateTerrain();
        }
    }

    for (auto it = terrain.begin(); it != terrain.end();) {
        sf::Sprite &r = (*it);
        r.move(speed * 0.3 * deltaTime, 0);

        if (r.getPosition().x + r.getGlobalBounds().width < 0.f) {
            terrain.erase(it);
        } else {
            ++it;
        }

    }

    base.move(speed*deltaTime, 0);
    if (base.getPosition().x + base.getGlobalBounds().width < 800) {
        base.setPosition(0, Config::HEIGHT - base.getGlobalBounds().height);
    }
}

int GroundManager::calcNextGeneration() {
    return rand() % 20;
}

void GroundManager::generateTerrain() {
    sf::Sprite s;
    int type = 1 + rand() % 8;

    switch (type) {
        case 1:
            s.setTexture(*ESE::Textures::instance().getResource("Ground1"));
            break;
        case 2:
            s.setTexture(*ESE::Textures::instance().getResource("Ground2"));
            break;
        case 3:
            s.setTexture(*ESE::Textures::instance().getResource("Ground3"));
            break;
        case 4:
            s.setTexture(*ESE::Textures::instance().getResource("Ground4"));
            break;
        case 5:
            s.setTexture(*ESE::Textures::instance().getResource("Mountain1"));
            break;
        case 6:
            s.setTexture(*ESE::Textures::instance().getResource("Mountain2"));
            break;
        case 7:
            s.setTexture(*ESE::Textures::instance().getResource("Mountain3"));
            break;
        case 8:
            s.setTexture(*ESE::Textures::instance().getResource("Mountain4"));
            break;
    }
    s.setPosition(Config::WIDTH, Config::HEIGHT - GROUND_HEIGHT - s.getGlobalBounds().height);
    terrain.push_back(s);
}

void GroundManager::setSpeed(float speed) {
    this->speed = speed;
}

float GroundManager::getSpeed() const {
    return speed;
}