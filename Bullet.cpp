/* 
 * File:   Bullet.cpp
 * Author: Rafa
 * 
 * Created on 4 de agosto de 2015, 18:01
 */

#include "Bullet.hpp"
#include <SFML/Graphics/RenderTarget.hpp>

Bullet::Bullet(float ySpeed) {
    representation.setFillColor(sf::Color::Black);
    representation.setSize(sf::Vector2f(3, 3));
    setSpeed(sf::Vector2f(900, ySpeed));
}

Bullet::~Bullet() {
}

void Bullet::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    states.transform *= this->getTransform();
    target.draw(representation, states);
}

void Bullet::advanceTime(float deltaTime) {
    move(getSpeed() * deltaTime);
}

sf::Vector2f Bullet::getSize() const {
    return sf::Vector2f(representation.getGlobalBounds().width, representation.getGlobalBounds().height);
}
