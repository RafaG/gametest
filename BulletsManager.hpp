/* 
 * File:   BulletsManager.hpp
 * Author: Rafa
 *
 * Created on 4 de agosto de 2015, 17:55
 */

#ifndef BULLETSMANAGER_HPP
#define	BULLETSMANAGER_HPP

#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Sound.hpp>

#include "Bullet.hpp"
#include "Enemy.hpp"
#include "ActorManager.hpp"

class ParticleEngine;

class BulletsManager : public ActorManager<Bullet>{
public:
    BulletsManager(ParticleEngine & particle);
    BulletsManager(const BulletsManager& orig);
    virtual ~BulletsManager();
    
    bool checkCollisions(ActorManager<Enemy>&actorManager, sf::Vector2f & retPosition);
    
protected:
    virtual bool isRemovable(Bullet *actor) const;
    
private:
    ParticleEngine & particleEngine;
    sf::SoundBuffer bShotHit;
    sf::Sound shotHit;
};

#endif	/* BULLETSMANAGER_HPP */

