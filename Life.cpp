/* 
 * File:   Life.cpp
 * Author: Mario
 * 
 * Created on 21 de julio de 2015, 1:05
 */

#include "Life.hpp"
#include "ESE/Core/Textures.hpp"
Life::Life() {
    carrot.setTexture(*ESE::Textures::instance().getResource("Carrot"));
    lifes = 0;
}
Life::~Life() {
}
void Life::draw(sf::RenderTarget & target, sf::RenderStates states) const {
    sf::Sprite copy = carrot;
    for(int i = 0; i < lifes;i++){   
       
        copy.setPosition(5+carrot.getGlobalBounds().width*i,5+carrot.getPosition().y);
        target.draw(copy,states); 
    }
    
}
 void Life::setLife(int lifes){
     this->lifes = lifes;
 }