/* 
 * File:   EntitiesManager.hpp
 * Author: Rafa
 *
 * Created on 31 de julio de 2015, 17:00
 */

#ifndef ENTITIESMANAGER_HPP
#define	ENTITIESMANAGER_HPP

#include <iostream>
#include <vector>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <ESE/Core/Animatable.hpp>
#include <SFML/System/Clock.hpp>

#include "ActorManager.hpp"
#include "Enemy.hpp"

class EnemiesManager : public ActorManager<Enemy> {
public:
    EnemiesManager();
    virtual ~EnemiesManager();
    void advanceTime(float deltaTime);
    
    void addCactus();
    void addBird();
    void addKamikazeBird();
    void setGroundHeight(float groundHeight);
    
protected:
    virtual bool isRemovable(Enemy*actor) const;
    
private:
    sf::Clock generationClock, birdsClock,kamikazeBirdsClock;
    float groundHeight;
    float variableTimeAmountForCactus;
};

#endif	/* ENTITIESMANAGER_HPP */

