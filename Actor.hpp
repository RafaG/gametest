/* 
 * File:   Player.hpp
 * Author: Mario
 *
 * Created on 17 de julio de 2015, 18:19
 */

#ifndef PLAYER_HPP
#define	PLAYER_HPP
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <ESE/Core/Animatable.hpp>
class Actor : public sf::Drawable, public sf::Transformable, public ESE::Animatable{
public:
    Actor();
    virtual ~Actor();
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states)const = 0;
    virtual void advanceTime(float deltaTime) = 0;
    virtual bool collidesWith(const Actor &other);
    virtual sf::Vector2f getSize() const = 0;
    virtual sf::Vector2f getSpeed()const;
    virtual void setSpeed(const sf::Vector2f &speed);
    virtual sf::FloatRect getCollidingRect() const;
 

protected:
    sf::Vector2f speed;
        
};

#endif	/* PLAYER_HPP */

