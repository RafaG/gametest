/* 
 * File:   Bird.cpp
 * Author: Mario
 * 
 * Created on 8 de agosto de 2015, 3:15
 */

#include "Bird.hpp"

Bird::Bird() : representation("Bird", 2, 0.3f){
    representation.resume();
}


Bird::~Bird() {
}
void Bird::draw(sf::RenderTarget & target, sf::RenderStates states) const{
    states.transform*=this->getTransform();
    target.draw(representation,states);
}
sf::Vector2f Bird::getSize() const {
    return sf::Vector2f(representation.getSize().x,representation.getSize().y);
}
void Bird::advanceTime(float deltaTime){
    representation.advanceTime(deltaTime);
    move(getSpeed()*deltaTime);
}