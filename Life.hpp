/* 
 * File:   Life.hpp
 * Author: Mario
 *
 * Created on 21 de julio de 2015, 1:05
 */

#ifndef LIFE_HPP
#define	LIFE_HPP
#include <SFML/Graphics.hpp>

class Life : public sf::Drawable {
public:
    Life();
    virtual ~Life();
    void draw(sf::RenderTarget & target, sf::RenderStates states) const;
    void setLife(int lifes);
   
private:
    sf::Sprite carrot;
    int lifes;
};

#endif	/* LIFE_HPP */

