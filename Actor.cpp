/* 
 * File:   Player.cpp
 * Author: Mario
 * 
 * Created on 17 de julio de 2015, 18:19
 */

#include "Actor.hpp"
#include <SFML/Graphics/RenderTarget.hpp>
#include <iostream>


Actor::Actor() {
   
}
Actor::~Actor() {
}

void Actor::setSpeed(const sf::Vector2f& speed){
    this->speed = speed;
}
sf::Vector2f Actor::getSpeed()const{
    return speed;
}

bool Actor::collidesWith(const Actor &other) {
    return getCollidingRect().intersects(other.getCollidingRect());
}

sf::FloatRect Actor::getCollidingRect() const {
    return sf::FloatRect(getPosition(),getSize());
}