/* 
 * File:   Enemy.hpp
 * Author: Mario
 *
 * Created on 18 de julio de 2015, 4:01
 */

#ifndef ENEMY_HPP
#define	ENEMY_HPP
#include "Actor.hpp"
#include <SFML/Graphics.hpp>

class Enemy : public Actor {
public:
    Enemy();
    virtual ~Enemy();
    void advanceTime(float deltaTime);
    void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;
    sf::Vector2f getSize() const = 0;
    virtual bool acceptBulletDamage() const = 0;

};

#endif	/* ENEMY_HPP */

