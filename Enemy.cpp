/* 
 * File:   Enemy.cpp
 * Author: Mario
 * 
 * Created on 18 de julio de 2015, 4:01
 */

#include "Enemy.hpp"

Enemy::Enemy() {
 }

Enemy::~Enemy() {
}

void Enemy::advanceTime(float deltaTime){
    move(getSpeed()*deltaTime);
}