/* 
 * File:   Score.hpp
 * Author: Mario
 *
 * Created on 2 de agosto de 2015, 5:06
 */

#ifndef SCORE_HPP
#define	SCORE_HPP
#include "SFML/Graphics.hpp"
#include "ESE/Core/Animatable.hpp"

class Score : public sf::Drawable,public ESE::Animatable {
public:
    Score();
    virtual ~Score();
    void advanceTime(float deltaTime);
    void draw(sf::RenderTarget & target, sf::RenderStates states) const;
    void setPosition(const sf::Vector2f & pos);
    void addScore(int amount, const sf::Vector2f & pos);
    void reset();

private:
    sf::Text scoreText;
    float score;
    
    bool extraScoreVisible;
    sf::Text extraScore;
    sf::Clock extraScoreClock;
};

#endif	/* SCORE_HPP */

