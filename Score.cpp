/* 
 * File:   Score.cpp
 * Author: Mario
 * 
 * Created on 2 de agosto de 2015, 5:06
 */

#include "Score.hpp"
#include <sstream>
#include <ESE/Core/Fonts.hpp>
Score::Score() {
    scoreText.setCharacterSize(35);
    scoreText.setColor(sf::Color::White);
    scoreText.setFont(*ESE::Fonts::instance().getResource("PixFont"));
    
    extraScore.setCharacterSize(20);
    extraScore.setColor(sf::Color::White);
    extraScore.setFont(*ESE::Fonts::instance().getResource("PixFont"));
    
    score = 0.f;
}
Score::~Score() {
}

void Score::advanceTime(float deltaTime) {
    score+=100*deltaTime;
    
    std::string target;
    std::stringstream ss;
    ss << (int)score;
    ss >> target;
    
    scoreText.setString(target);
    
    if (extraScoreVisible) {
        if (extraScoreClock.getElapsedTime().asMilliseconds()<500) {
            extraScore.move(0,-250*deltaTime);
        }
        else {
            extraScoreVisible = false;
        }
    }
    
    
    
}
void Score::draw(sf::RenderTarget & target, sf::RenderStates states) const{
      target.draw(scoreText, states);
      if (extraScoreVisible) {
          target.draw(extraScore, states);
      }
}
void Score::setPosition(const sf::Vector2f & pos){
    scoreText.setPosition(pos);
}

void Score::addScore(int amount, const sf::Vector2f & pos) {
    score+=amount;
    extraScoreVisible = true;
    extraScore.setPosition(pos);
    
    std::string target;
    std::stringstream ss;
    ss << amount;
    ss >> target;
    
    extraScore.setString("+"+target);
    extraScoreClock.restart();
}

void Score::reset() {
    score = 0;
}