/* 
 * File:   EntitiesManager.cpp
 * Author: Rafa
 * 
 * Created on 31 de julio de 2015, 17:00
 */

#include "EnemiesManager.hpp"
#include <iostream>
#include "Enemy.hpp"
#include "Cactus.hpp"
#include "Bird.hpp"
#include "Config.hpp"
#include "kamikazeBird.h"

EnemiesManager::EnemiesManager() {
    groundHeight = 0.f;
    variableTimeAmountForCactus = (rand() % 15) / 10.f;
}

EnemiesManager::~EnemiesManager() {

}

void EnemiesManager::advanceTime(float deltaTime) {
    ActorManager<Enemy>::advanceTime(deltaTime);

    if (generationClock.getElapsedTime().asSeconds() > 1.f + variableTimeAmountForCactus) {
        addCactus();
        generationClock.restart();
        variableTimeAmountForCactus = (rand() % 15) / 10.f;
    }

    if (birdsClock.getElapsedTime().asSeconds() > 6.f) {
        addBird();
        birdsClock.restart();
    }
    if(kamikazeBirdsClock.getElapsedTime().asSeconds() > 3.f)
    {
        addKamikazeBird();
        kamikazeBirdsClock.restart();
    }
}

void EnemiesManager::addCactus() {
    Enemy * enemy = new Cactus();

    enemy->setPosition(800, Config::HEIGHT - enemy->getSize().y - groundHeight);
    enemy->setSpeed(getSpeed());

    addActor(enemy);
}

void EnemiesManager::addBird() {
    Enemy * enemy = new Bird();

    enemy->setPosition(800, 300);
    enemy->setSpeed(sf::Vector2f(-50,0));

    addActor(enemy);
}

void EnemiesManager::addKamikazeBird(){
    Enemy * enemy = new KamikazeBird;
    
    enemy->setPosition(800,300);
    enemy->setSpeed(sf::Vector2f(-50,0));
    
    addActor(enemy);
    
}

void EnemiesManager::setGroundHeight(float groundHeight) {
    this->groundHeight = groundHeight;
}

bool EnemiesManager::isRemovable(Enemy* actor) const {
    bool isRemovable = false;
    if(actor->getPosition().x + actor->getSize().x < 0) isRemovable = true;
    if(actor->getPosition().y + actor->getSize().y < 0) isRemovable = true;
    return (isRemovable);
}