/* 
 * File:   ActorManager.cpp
 * Author: Rafa
 * 
 * Created on 4 de agosto de 2015, 14:08
 */

#include "ActorManager.hpp"
#include <SFML/Graphics/RenderTarget.hpp>

template <class T>
ActorManager<T>::ActorManager() {
}

template <class T>
ActorManager<T>::~ActorManager() {
    for (auto actor : actors) {
        delete actor;
    }
}

template <class T>
void ActorManager<T>::draw(sf::RenderTarget & target, sf::RenderStates states) const {
    states.transform*=getTransform();
    for (auto it = actors.begin(); it!=actors.end(); ++it) {
        target.draw(**it,states);
    }
}

template <class T>
void ActorManager<T>::advanceTime(float deltaTime) {
    for (auto it = actors.begin(); it!=actors.end();) {
        (*it)->advanceTime(deltaTime);
        
        if (isRemovable(*it)) {
            actors.erase(it);
        }
        else {
            ++it;
        }
    }
}

template <class T>
sf::Vector2f ActorManager<T>::getSize() const {
    return sf::Vector2f(0,0);
}

template <class T>
bool ActorManager<T>::collidesWith(const Actor& actor) {
    return getIteratorActorCollidingWith(actor)!=actors.end();
}

template <class T>
typename std::vector<T*>::iterator ActorManager<T>::getIteratorActorCollidingWith(const Actor& actor) {
    for (auto it = actors.begin(); it!=actors.end(); ++it) {
        if ((*it)->collidesWith(actor)) {
            return it;
        }
    }
    return actors.end();
}

template <class T>
void ActorManager<T>::addActor(T* actor) {
    actors.push_back(actor);
}

template <class T>
typename std::vector<T*>::iterator ActorManager<T>::end() {
    return actors.end();
}

template <class T>
void ActorManager<T>::remove(typename std::vector<T*>::iterator it) {
    delete *it;
    actors.erase(it);
}