/* 
 * File:   MenuScene.hpp
 * Author: Rafa
 *
 * Created on 18 de julio de 2015, 1:57
 */

#ifndef MENUSCENE_HPP
#define	MENUSCENE_HPP

#include <ESE/Core/Scene.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include "Animation.hpp"

class MenuScene : public ESE::Scene {
public:
    MenuScene(sf::RenderWindow * window);
    virtual ~MenuScene();
    
protected:
    void logic(float deltaTime);
    void manageEvents(float deltaTime);
    void render();
private:
    static const int MAX_OPTIONS = 3;
    
    sf::Sprite background, spiral;
    Animation rabbit;
    sf::Text option;
    sf::String options[MAX_OPTIONS];
    int currOption;
    
    void nextOption();
    void previousOption();
    void updateOption();
};

#endif	/* MENUSCENE_HPP */

