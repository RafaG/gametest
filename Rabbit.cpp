/* 
 * File:   Rabbit.cpp
 * Author: Mario
 * 
 * Created on 19 de julio de 2015, 0:53
 */

#include "Rabbit.hpp"
#include <ESE/Core/Textures.hpp>
#include "Config.hpp"

const float Rabbit::GRAVITY = 1400;

Rabbit::Rabbit() :
representation("Rabbit", 6, 0.08),
GROUND_HEIGHT(20) {
    jumping = false;
    representation.resume();
    life = maxLife = 0;

    bJump.loadFromFile("jump2.wav");
    soundJump.setBuffer(bJump);

    bShoot.loadFromFile("shoot1.wav");
    soundShoot.setBuffer(bShoot);

    godModeOn = false;
    shooting = false;

    shotgun.setTexture(*ESE::Textures::instance().getResource("Shotgun"));
}

Rabbit::~Rabbit() {
}

void Rabbit::advanceTime(float deltaTime) {

    if (isGod()) {
        if (godModeBlink.getElapsedTime().asMilliseconds() > 150) {
            representation.setAlpha(255);
            godModeBlink.restart();
        } else if (godModeBlink.getElapsedTime().asMilliseconds() > 75) {
            representation.setAlpha(50);
        }

        if (godModeClock.getElapsedTime().asSeconds() > 3.f) {
            godModeOn = false;
            representation.setAlpha(255);
        }
    }

    representation.advanceTime(deltaTime);
    if (jumping) {
        bool bNegative = speed.y < 0;
        speed.y += GRAVITY*deltaTime;
        if (bNegative && speed.y > 0) {
            representation.setFrame(4);
            representation.restartClock();
            representation.resume();
            representation.setFrameTime(0.3);
        }

        move(speed * deltaTime);
        if (getPosition().y > Config::HEIGHT - representation.getSize().y - GROUND_HEIGHT) {
            jumping = false;
            representation.resume();
            representation.setFrameTime(0.08);
        }
    }

    if (isShooting()) {
        if (soundShoot.getStatus() == sf::Sound::Stopped) {
            shooting = false;
            if (isJumping()) {
                representation.setFrame(lastFrameBeforeShooting);
            } else {
                representation.setFrame(0);
            }
            representation.restartClock();
            representation.resume();
        }
    }
}

void Rabbit::jump(float pSpeed) {
    if (!jumping) {
        speed.y = -pSpeed;
        jumping = true;
        representation.setFrame(3);
        representation.pause();
        soundJump.play();
    }

}

bool Rabbit::isJumping() const {
    return jumping;
}

void Rabbit::draw(sf::RenderTarget & target, sf::RenderStates states) const {
    states.transform *= this->getTransform();
    if (shooting) {
        target.draw(shotgun, states);
    }
    target.draw(representation, states);
}

sf::Vector2f Rabbit::getSize() const {
    return representation.getSize();
}

int Rabbit::getLife() const {
    return life;
}

void Rabbit::setLife(int life) {
    this->life = life;
}

void Rabbit::setMaxLife(int maxLife) {
    this->maxLife = maxLife;
}

void Rabbit::increaseLife() {
    if (life < maxLife) life++;
}

void Rabbit::decreaseLife() {
    if (life > 0 && !isGod()) {
        life--;
        startGodMode();
    }
}

bool Rabbit::isLifeFull() const {
    return life == maxLife;
}

bool Rabbit::isDead() {
    return life == 0;
}

void Rabbit::reset() {
    life = maxLife;
}

void Rabbit::shoot() {
    if (!isShooting()) {
        lastFrameBeforeShooting = representation.getCurrentFrame();
        shooting = true;
        shootingClock.restart();
        if (isJumping()) {
            representation.setFrame(2);
        } else {
            representation.setFrame(1);
        }
        representation.pause();

        shotgun.setPosition(representation.getPosition() + sf::Vector2f(28, 52));

        soundShoot.play();
    }
}

bool Rabbit::isShooting() const {
    return shooting;
}

sf::Vector2f Rabbit::getBulletOutPosition() const {
    return getPosition() + shotgun.getPosition() + sf::Vector2f(shotgun.getGlobalBounds().width, 0);
}

void Rabbit::startGodMode() {
    godModeClock.restart();
    godModeBlink.restart();
    godModeOn = true;
}

bool Rabbit::isGod() const {
    return godModeOn;
}