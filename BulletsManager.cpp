/* 
 * File:   BulletsManager.cpp
 * Author: Rafa
 * 
 * Created on 4 de agosto de 2015, 17:55
 */

#include "BulletsManager.hpp"
#include "ParticleEngine/ParticleEngine.hpp"
#include "ParticleEngine/Blood.hpp"
#include <iostream>

BulletsManager::BulletsManager(ParticleEngine & particleEngine) : particleEngine(particleEngine) {
    bShotHit.loadFromFile("shothit.wav");
    shotHit.setBuffer(bShotHit);
}

BulletsManager::~BulletsManager() {
}

bool BulletsManager::isRemovable(Bullet *actor) const {
    return actor->getPosition().x>800;
}

bool BulletsManager::checkCollisions(ActorManager<Enemy>& actorManager, sf::Vector2f & retPosition) {
    for (auto it = actors.begin(); it!=actors.end();) {
        
        auto i = actorManager.getIteratorActorCollidingWith(*(*it));
        
        if (i!=actorManager.end() && (*i)->acceptBulletDamage()) {
            particleEngine.addGenerator(new Blood((*it)->getPosition(),particleEngine));
            this->remove(it);
            
            retPosition.x = (*i)->getPosition().x;
            retPosition.y = (*i)->getPosition().y;
            
            actorManager.remove(i);
            shotHit.play();
            return true;
            
        }
        else{
            ++it;
        }
    }
}