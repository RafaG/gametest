/* 
 * File:   Cactus.hpp
 * Author: Rafa
 *
 * Created on 2 de agosto de 2015, 16:09
 */

#ifndef CACTUS_HPP
#define CACTUS_HPP

#include "Enemy.hpp"

class Cactus : public Enemy {
public:
    Cactus();
    virtual ~Cactus();
    sf::FloatRect getCollidingRect() const;

    bool acceptBulletDamage() const {
        return false;
    }
    void draw(sf::RenderTarget & target, sf::RenderStates states) const;
    sf::Vector2f getSize() const;
private:
    sf::Sprite representation;
};

#endif /* CACTUS_HPP */

