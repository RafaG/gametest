/* 
 * File:   SetupGameSessionScene.cpp
 * Author: Rafa
 * 
 * Created on 3 de agosto de 2015, 15:30
 */

#include "SetupGameSessionScene.hpp"

SetupGameSessionScene::SetupGameSessionScene() {
}


SetupGameSessionScene::~SetupGameSessionScene() {
}

void SetupGameSessionScene::logic(float deltaTime) {
}

void SetupGameSessionScene::manageEvents(float deltaTime) {
    ESE::Scene::manageEvents(deltaTime);
    
}

void SetupGameSessionScene::render() {
}
