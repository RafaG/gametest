/* 
 * File:   SetupGameSessionScene.hpp
 * Author: Rafa
 *
 * Created on 3 de agosto de 2015, 15:30
 */

#ifndef SETUPGAMESESSIONSCENE_HPP
#define	SETUPGAMESESSIONSCENE_HPP

#include <ESE/Core/Scene.hpp>

class SetupGameSessionScene : public ESE::Scene{
public:
    SetupGameSessionScene();
    virtual ~SetupGameSessionScene();
protected:
    void logic(float deltaTime);
    void manageEvents(float deltaTime);
    void render();
    
private:

};

#endif	/* SETUPGAMESESSIONSCENE_HPP */

