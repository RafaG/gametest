/* 
 * File:   Bullet.hpp
 * Author: Rafa
 *
 * Created on 4 de agosto de 2015, 18:01
 */

#ifndef BULLET_HPP
#define	BULLET_HPP

#include "Actor.hpp"
#include <SFML/Graphics/RectangleShape.hpp>

class Bullet : public Actor{
public:
    Bullet(float ySpeed = 0.f);
    virtual ~Bullet();
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states)const;
    virtual void advanceTime(float deltaTime);
    virtual sf::Vector2f getSize() const;
private:
    sf::RectangleShape representation;
};

#endif	/* BULLET_HPP */

