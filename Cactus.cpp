/* 
 * File:   Cactus.cpp
 * Author: Rafa
 * 
 * Created on 2 de agosto de 2015, 16:09
 */

#include "Cactus.hpp"
#include <ESE/Core/Textures.hpp>

Cactus::Cactus() {
    representation.setTexture(*ESE::Textures::instance().getResource("Cactus"));
}

Cactus::~Cactus() {
}

sf::FloatRect Cactus::getCollidingRect() const {
    sf::FloatRect original = Actor::getCollidingRect();
    original.left*=1.1;
    original.width*=0.8;
    original.top*=1.1;
    original.height*=0.9;
    
    return original;
}

void Cactus::draw(sf::RenderTarget & target, sf::RenderStates states) const {
    states.transform*=this->getTransform();
    target.draw(representation,states);
}

sf::Vector2f Cactus::getSize() const {
    return sf::Vector2f(representation.getGlobalBounds().width,representation.getGlobalBounds().height);
}