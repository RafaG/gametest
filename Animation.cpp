/* 
 * File:   Animation.cpp
 * Author: Rafa
 * 
 * Created on 24 de junio de 2015, 15:09
 */

#include "Animation.hpp"
#include "ESE/Core/Textures.hpp"

Animation::Animation(std::string resourceName, int horizontalFrames, float secondsPerFrame) {
    sprite.setTexture(*ESE::Textures::instance().getResource(resourceName));
    currentFrame = 0;
    this->secondsPerFrame = secondsPerFrame;
    paused = true;
    
    this->horizontalFrames = horizontalFrames;
    startFrame = 0;
    endFrame = horizontalFrames-1;
    int w = sprite.getGlobalBounds().width/horizontalFrames;
    
    for (int i = 0; i<horizontalFrames; ++i) {
        rects.push_back(sf::IntRect(w*i,0,w,sprite.getGlobalBounds().height));
    }
    
    sprite.setTextureRect(rects[0]);
}

Animation::~Animation() {
}

void Animation::draw(sf::RenderTarget & renderTarget, sf::RenderStates states) const{
    renderTarget.draw(sprite,states);
}

void Animation::advanceTime(float deltaTime) {
    if (!paused) {
        if (clock.getElapsedTime().asSeconds()>secondsPerFrame) {
            clock.restart();
            nextFrame();
        }
    }
}

void Animation::nextFrame() {
    currentFrame++;
    if (currentFrame>endFrame) {
        currentFrame = startFrame;
    }
    sprite.setTextureRect(rects[currentFrame]);
}

void Animation::setFrame(int frame) {
    currentFrame = frame;
    sprite.setTextureRect(rects[currentFrame]);
}

sf::Vector2f Animation::getSize() const {
    return sf::Vector2f(sprite.getGlobalBounds().width,sprite.getGlobalBounds().height);
}

void Animation::pause() {
    paused = true;
}

void Animation::resume() {
    if (paused) {
        paused = false;
        //clock.restart();
    }
}

void Animation::setFramesRange(int start, int end) {
    startFrame = start;
    endFrame = end;
    if (currentFrame<startFrame) {
        setFrame(startFrame);
    }
    else if (currentFrame>endFrame) {
        setFrame(endFrame);
    }
}

void Animation::restartClock() {
    clock.restart();
}

void Animation::scale(float x, float y) {
    sprite.scale(x,y);
}

void Animation::setScale(float x, float y) {
    sprite.setScale(x,y);
}

void Animation::move(float x, float y) {
    sprite.move(x,y);
}

void Animation::move(const sf::Vector2f & mov) {
    sprite.move(mov);
}

void Animation::setPosition(const sf::Vector2f & pos) {
    sprite.setPosition(pos);
}

sf::Vector2f Animation::getPosition() const {
    return sprite.getPosition();
}

void Animation::setFrameTime(float second) {
    this->secondsPerFrame = second;
}

float Animation::getFrameTime() const {
    return secondsPerFrame;
}

int Animation::getCurrentFrame() const {
    return currentFrame;
}

void Animation::setAlpha(int alpha) {
    sprite.setColor(sf::Color(255,255,255,alpha));
}