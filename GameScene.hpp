/* 
 * File:   GameScene.hpp
 * Author: Rafa
 *
 * Created on 17 de julio de 2015, 17:30
 */

#ifndef GAMESCENE_HPP
#define	GAMESCENE_HPP

#include <ESE/Core/Scene.hpp>
#include <SFML/Audio.hpp>

#include "GroundManager.hpp"
#include "EnemiesManager.hpp"
#include "CarrotManager.hpp"
#include "BulletsManager.hpp"
#include "ParticleEngine/ParticleEngine.hpp"

#include "Rabbit.hpp"
#include "Life.hpp"
#include "Score.hpp"
#include "Bird.hpp"

class GameScene : public ESE::Scene {
public:
    GameScene(const std::string & name, sf::RenderWindow *window);
    virtual ~GameScene();
    void logic(float deltaTime);
    void render();
    void manageEvents(float deltaTime);
private:
    
    void updateSpeed();
    
    Rabbit player;
    Life life;
    Score score;
    float speed;
    
    GroundManager groundManager;
    EnemiesManager entitiesManager;
    CarrotManager carrotManager;
    BulletsManager bulletsManager;
    ParticleEngine particleEngine;
    
    
    
    sf::Clock jumpPressingClock;
    bool checkLongJump;
    
    sf::SoundBuffer hit;
    sf::Sound hitting;
    
    sf::SoundBuffer takeLife;
    sf::Sound takingLife;
    
    bool shootBtnReleased;
};

#endif	/* GAMESCENE_HPP */

