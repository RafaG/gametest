/* 
 * File:   Carrot.hpp
 * Author: Mario
 *
 * Created on 28 de julio de 2015, 23:10
 */

#ifndef CARROT_HPP
#define	CARROT_HPP
#include <SFML/Graphics.hpp>
#include "Actor.hpp"
class Carrot : public Actor{
public:
  
    Carrot();
    virtual ~Carrot();
    void advanceTime(float deltaTime);
    void draw(sf::RenderTarget& target, sf::RenderStates states)const;
    sf::Vector2f getSize() const;
 
private:
    sf::Texture carrot;
    sf::Sprite representCarrot;
};

#endif	/* CARROT_HPP */

