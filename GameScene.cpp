/* 
 * File:   GameScene.cpp
 * Author: Rafa
 * 
 * Created on 17 de julio de 2015, 17:30
 */

#include "GameScene.hpp"
#include "CarrotManager.hpp"
#include <iostream>
#include <ESE/Core/ResourceManager.hpp>
#include <sstream>
#include "ParticleEngine/Rain.hpp"

GameScene::GameScene(const std::string & name, sf::RenderWindow *window) :
ESE::Scene(name, window), carrotManager(200, window->getSize().y - player.GROUND_HEIGHT),
bulletsManager(particleEngine) {

    player.setPosition(sf::Vector2f(80, window->getSize().y - player.getSize().y - player.GROUND_HEIGHT));
    player.setMaxLife(4);
    player.reset();

    hit.loadFromFile("hit.wav");
    hitting.setBuffer(hit);

    speed = -300.f;

    updateSpeed();

    particleEngine.addGenerator(new Rain(particleEngine));

    entitiesManager.setGroundHeight(player.GROUND_HEIGHT);

    checkLongJump = false;

    takeLife.loadFromFile("life2.wav");
    takingLife.setBuffer(takeLife);

    score.setPosition(sf::Vector2f(300, 5));

    shootBtnReleased = true;

}

GameScene::~GameScene() {
}

void GameScene::logic(float deltaTime) {
    groundManager.advanceTime(deltaTime);
    player.advanceTime(deltaTime);
    entitiesManager.advanceTime(deltaTime);
    carrotManager.advanceTime(deltaTime);
    score.advanceTime(deltaTime);
    bulletsManager.advanceTime(deltaTime);
    particleEngine.advanceTime(deltaTime);

    sf::Vector2f position;
    if (bulletsManager.checkCollisions(entitiesManager, position)) {
        score.addScore(1500, position);
    }

    life.setLife(player.getLife());

    if (!player.isGod() && entitiesManager.collidesWith(player)) {
        player.decreaseLife();
        hitting.play();
    }

    if (carrotManager.collidesWith(player)) {
        if (player.isLifeFull()) {
            score.addScore(1500, player.getPosition());
        } else {
            player.increaseLife();
        }
        takingLife.play();

    }
}

void GameScene::render() {
    window->clear(sf::Color(107, 174, 186));
    draw(groundManager);
    draw(player);
    draw(entitiesManager);
    draw(carrotManager);
    draw(bulletsManager);
    draw(particleEngine);
    draw(life);
    draw(score);
}

void GameScene::manageEvents(float deltaTime) {
    ESE::Scene::manageEvents(deltaTime);

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
        if (!player.isJumping()) {
            player.jump();
            jumpPressingClock.restart();
            checkLongJump = true;

        }

        if (checkLongJump && player.isJumping() && jumpPressingClock.getElapsedTime().asMilliseconds() > 100) {
            player.setSpeed(sf::Vector2f(0, -900));
            checkLongJump = false;
        }
    } else {
        if (checkLongJump && jumpPressingClock.getElapsedTime().asMilliseconds() > 100) {
            checkLongJump = false;
        }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::C)) {
        carrotManager.addCarrot(200, window->getSize().y - player.GROUND_HEIGHT);
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::RControl)) {
        if (shootBtnReleased && !player.isShooting()) {
            player.shoot();
            shootBtnReleased = false;

            const int amount = 7;
            float initialYSpeed = -200;
            float incr = (fabs(initialYSpeed) * 2) / amount;

            for (int i = 0; i < amount; i++) {
                Bullet *b = new Bullet(initialYSpeed + incr * i);
                b->setPosition(player.getBulletOutPosition());
                bulletsManager.addActor(b);
            }
        }
    } else {
        shootBtnReleased = true;
    }
}

void GameScene::updateSpeed() {
    groundManager.setSpeed(speed);
    entitiesManager.setSpeed(sf::Vector2f(groundManager.getSpeed(), 0.f));
    carrotManager.setSpeed(sf::Vector2f(groundManager.getSpeed(), 0.f));
    particleEngine.setSpeed(sf::Vector2f(groundManager.getSpeed(), 0.f));
}
