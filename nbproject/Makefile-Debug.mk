#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW4.9.2-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/Actor.o \
	${OBJECTDIR}/Animation.o \
	${OBJECTDIR}/Bird.o \
	${OBJECTDIR}/Bullet.o \
	${OBJECTDIR}/BulletsManager.o \
	${OBJECTDIR}/_ext/d78585b2/AnimatableContainer.o \
	${OBJECTDIR}/_ext/d78585b2/Application.o \
	${OBJECTDIR}/_ext/d78585b2/Clock.o \
	${OBJECTDIR}/_ext/d78585b2/Layer.o \
	${OBJECTDIR}/_ext/d78585b2/Log.o \
	${OBJECTDIR}/_ext/d78585b2/RenderManager.o \
	${OBJECTDIR}/_ext/d78585b2/Scene.o \
	${OBJECTDIR}/_ext/d78585b2/SceneManager.o \
	${OBJECTDIR}/_ext/d78585b2/ScreenDimensions.o \
	${OBJECTDIR}/_ext/83449f05/Entity.o \
	${OBJECTDIR}/_ext/83449f05/System.o \
	${OBJECTDIR}/_ext/9880f85e/pugixml.o \
	${OBJECTDIR}/_ext/d789ec00/Dijkstra.o \
	${OBJECTDIR}/_ext/d789ec00/Mesh.o \
	${OBJECTDIR}/_ext/d789ec00/Node.o \
	${OBJECTDIR}/_ext/d789ec00/NodeDistance.o \
	${OBJECTDIR}/_ext/d789ec00/NodeLink.o \
	${OBJECTDIR}/_ext/1f4af354/AABBDetection.o \
	${OBJECTDIR}/_ext/1f4af354/CircleCollisionDetection.o \
	${OBJECTDIR}/_ext/d78d1b40/Text.o \
	${OBJECTDIR}/_ext/909eba63/TileDrawable.o \
	${OBJECTDIR}/_ext/909eba63/TilemapLoader.o \
	${OBJECTDIR}/Cactus.o \
	${OBJECTDIR}/Carrot.o \
	${OBJECTDIR}/CarrotManager.o \
	${OBJECTDIR}/EnemiesManager.o \
	${OBJECTDIR}/Enemy.o \
	${OBJECTDIR}/GameScene.o \
	${OBJECTDIR}/GroundManager.o \
	${OBJECTDIR}/Life.o \
	${OBJECTDIR}/MenuScene.o \
	${OBJECTDIR}/ParticleEngine/Blood.o \
	${OBJECTDIR}/ParticleEngine/Drop.o \
	${OBJECTDIR}/ParticleEngine/Particle.o \
	${OBJECTDIR}/ParticleEngine/ParticleEngine.o \
	${OBJECTDIR}/ParticleEngine/ParticleGenerator.o \
	${OBJECTDIR}/ParticleEngine/Rain.o \
	${OBJECTDIR}/Rabbit.o \
	${OBJECTDIR}/Score.o \
	${OBJECTDIR}/kamikazeBird.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-L/C/SFML-2.3.2/lib -lsfml-audio-d -lsfml-graphics-d -lsfml-system-d -lsfml-window-d

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/gametest.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/gametest.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/gametest ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/Actor.o: Actor.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Actor.o Actor.cpp

${OBJECTDIR}/Animation.o: Animation.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Animation.o Animation.cpp

${OBJECTDIR}/Bird.o: Bird.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Bird.o Bird.cpp

${OBJECTDIR}/Bullet.o: Bullet.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Bullet.o Bullet.cpp

${OBJECTDIR}/BulletsManager.o: BulletsManager.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BulletsManager.o BulletsManager.cpp

${OBJECTDIR}/_ext/d78585b2/AnimatableContainer.o: /C/ESE/src/ESE/Core/AnimatableContainer.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d78585b2
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d78585b2/AnimatableContainer.o /C/ESE/src/ESE/Core/AnimatableContainer.cpp

${OBJECTDIR}/_ext/d78585b2/Application.o: /C/ESE/src/ESE/Core/Application.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d78585b2
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d78585b2/Application.o /C/ESE/src/ESE/Core/Application.cpp

${OBJECTDIR}/_ext/d78585b2/Clock.o: /C/ESE/src/ESE/Core/Clock.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d78585b2
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d78585b2/Clock.o /C/ESE/src/ESE/Core/Clock.cpp

${OBJECTDIR}/_ext/d78585b2/Layer.o: /C/ESE/src/ESE/Core/Layer.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d78585b2
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d78585b2/Layer.o /C/ESE/src/ESE/Core/Layer.cpp

${OBJECTDIR}/_ext/d78585b2/Log.o: /C/ESE/src/ESE/Core/Log.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d78585b2
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d78585b2/Log.o /C/ESE/src/ESE/Core/Log.cpp

${OBJECTDIR}/_ext/d78585b2/RenderManager.o: /C/ESE/src/ESE/Core/RenderManager.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d78585b2
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d78585b2/RenderManager.o /C/ESE/src/ESE/Core/RenderManager.cpp

${OBJECTDIR}/_ext/d78585b2/Scene.o: /C/ESE/src/ESE/Core/Scene.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d78585b2
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d78585b2/Scene.o /C/ESE/src/ESE/Core/Scene.cpp

${OBJECTDIR}/_ext/d78585b2/SceneManager.o: /C/ESE/src/ESE/Core/SceneManager.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d78585b2
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d78585b2/SceneManager.o /C/ESE/src/ESE/Core/SceneManager.cpp

${OBJECTDIR}/_ext/d78585b2/ScreenDimensions.o: /C/ESE/src/ESE/Core/ScreenDimensions.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d78585b2
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d78585b2/ScreenDimensions.o /C/ESE/src/ESE/Core/ScreenDimensions.cpp

${OBJECTDIR}/_ext/83449f05/Entity.o: /C/ESE/src/ESE/EntitySystem/Entity.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/83449f05
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/83449f05/Entity.o /C/ESE/src/ESE/EntitySystem/Entity.cpp

${OBJECTDIR}/_ext/83449f05/System.o: /C/ESE/src/ESE/EntitySystem/System.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/83449f05
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/83449f05/System.o /C/ESE/src/ESE/EntitySystem/System.cpp

${OBJECTDIR}/_ext/9880f85e/pugixml.o: /C/ESE/src/ESE/External/pugixml.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/9880f85e
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/9880f85e/pugixml.o /C/ESE/src/ESE/External/pugixml.cpp

${OBJECTDIR}/_ext/d789ec00/Dijkstra.o: /C/ESE/src/ESE/Mesh/Dijkstra.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d789ec00
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d789ec00/Dijkstra.o /C/ESE/src/ESE/Mesh/Dijkstra.cpp

${OBJECTDIR}/_ext/d789ec00/Mesh.o: /C/ESE/src/ESE/Mesh/Mesh.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d789ec00
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d789ec00/Mesh.o /C/ESE/src/ESE/Mesh/Mesh.cpp

${OBJECTDIR}/_ext/d789ec00/Node.o: /C/ESE/src/ESE/Mesh/Node.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d789ec00
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d789ec00/Node.o /C/ESE/src/ESE/Mesh/Node.cpp

${OBJECTDIR}/_ext/d789ec00/NodeDistance.o: /C/ESE/src/ESE/Mesh/NodeDistance.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d789ec00
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d789ec00/NodeDistance.o /C/ESE/src/ESE/Mesh/NodeDistance.cpp

${OBJECTDIR}/_ext/d789ec00/NodeLink.o: /C/ESE/src/ESE/Mesh/NodeLink.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d789ec00
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d789ec00/NodeLink.o /C/ESE/src/ESE/Mesh/NodeLink.cpp

${OBJECTDIR}/_ext/1f4af354/AABBDetection.o: /C/ESE/src/ESE/Physics/AABBDetection.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1f4af354
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1f4af354/AABBDetection.o /C/ESE/src/ESE/Physics/AABBDetection.cpp

${OBJECTDIR}/_ext/1f4af354/CircleCollisionDetection.o: /C/ESE/src/ESE/Physics/CircleCollisionDetection.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1f4af354
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1f4af354/CircleCollisionDetection.o /C/ESE/src/ESE/Physics/CircleCollisionDetection.cpp

${OBJECTDIR}/_ext/d78d1b40/Text.o: /C/ESE/src/ESE/Text/Text.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d78d1b40
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d78d1b40/Text.o /C/ESE/src/ESE/Text/Text.cpp

${OBJECTDIR}/_ext/909eba63/TileDrawable.o: /C/ESE/src/ESE/TileEngine/TileDrawable.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/909eba63
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/909eba63/TileDrawable.o /C/ESE/src/ESE/TileEngine/TileDrawable.cpp

${OBJECTDIR}/_ext/909eba63/TilemapLoader.o: /C/ESE/src/ESE/TileEngine/TilemapLoader.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/909eba63
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/909eba63/TilemapLoader.o /C/ESE/src/ESE/TileEngine/TilemapLoader.cpp

${OBJECTDIR}/Cactus.o: Cactus.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Cactus.o Cactus.cpp

${OBJECTDIR}/Carrot.o: Carrot.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Carrot.o Carrot.cpp

${OBJECTDIR}/CarrotManager.o: CarrotManager.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CarrotManager.o CarrotManager.cpp

${OBJECTDIR}/EnemiesManager.o: EnemiesManager.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/EnemiesManager.o EnemiesManager.cpp

${OBJECTDIR}/Enemy.o: Enemy.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Enemy.o Enemy.cpp

${OBJECTDIR}/GameScene.o: GameScene.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GameScene.o GameScene.cpp

${OBJECTDIR}/GroundManager.o: GroundManager.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GroundManager.o GroundManager.cpp

${OBJECTDIR}/Life.o: Life.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Life.o Life.cpp

${OBJECTDIR}/MenuScene.o: MenuScene.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MenuScene.o MenuScene.cpp

${OBJECTDIR}/ParticleEngine/Blood.o: ParticleEngine/Blood.cpp 
	${MKDIR} -p ${OBJECTDIR}/ParticleEngine
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ParticleEngine/Blood.o ParticleEngine/Blood.cpp

${OBJECTDIR}/ParticleEngine/Drop.o: ParticleEngine/Drop.cpp 
	${MKDIR} -p ${OBJECTDIR}/ParticleEngine
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ParticleEngine/Drop.o ParticleEngine/Drop.cpp

${OBJECTDIR}/ParticleEngine/Particle.o: ParticleEngine/Particle.cpp 
	${MKDIR} -p ${OBJECTDIR}/ParticleEngine
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ParticleEngine/Particle.o ParticleEngine/Particle.cpp

${OBJECTDIR}/ParticleEngine/ParticleEngine.o: ParticleEngine/ParticleEngine.cpp 
	${MKDIR} -p ${OBJECTDIR}/ParticleEngine
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ParticleEngine/ParticleEngine.o ParticleEngine/ParticleEngine.cpp

${OBJECTDIR}/ParticleEngine/ParticleGenerator.o: ParticleEngine/ParticleGenerator.cpp 
	${MKDIR} -p ${OBJECTDIR}/ParticleEngine
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ParticleEngine/ParticleGenerator.o ParticleEngine/ParticleGenerator.cpp

${OBJECTDIR}/ParticleEngine/Rain.o: ParticleEngine/Rain.cpp 
	${MKDIR} -p ${OBJECTDIR}/ParticleEngine
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ParticleEngine/Rain.o ParticleEngine/Rain.cpp

${OBJECTDIR}/Rabbit.o: Rabbit.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Rabbit.o Rabbit.cpp

${OBJECTDIR}/Score.o: Score.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Score.o Score.cpp

${OBJECTDIR}/kamikazeBird.o: kamikazeBird.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/kamikazeBird.o kamikazeBird.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/C/SFML-2.3.2/include -I/C/ESE/include/ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/gametest.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
