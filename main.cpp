/* 
 * File:   main.cpp
 * Author: Rafa
 *
 * Created on 17 de julio de 2015, 2:20
 */

#include <cstdlib>
#include <SFML/Graphics.hpp>
#include <ESE/Core/Application.hpp>
#include <ESE/Core/Scene.hpp>
#include <ESE/Core/Textures.hpp>
#include <ESE/Core/Fonts.hpp>

#include "GameScene.hpp"
#include "MenuScene.hpp"
#include "Config.hpp"
#include "ESE/Text/Text.hpp"

using namespace std;

class MainApplication : public ESE::Application {
private:
    ESE::Scene * gameScene, *menuScene;
public:
    MainApplication();
    virtual ~MainApplication();
};

MainApplication::MainApplication() {
    ESE::Textures::instance().loadFromFile("MenuBackground", "menubackground.png");
    ESE::Textures::instance().loadFromFile("MenuBackground", "menubackground.png");
    ESE::Textures::instance().loadFromFile("Spiral", "spiral.png");
    ESE::Textures::instance().loadFromFile("Rabbit", "rabbit.png");
    ESE::Textures::instance().loadFromFile("Cactus", "cactus.png");
    ESE::Textures::instance().loadFromFile("Shotgun", "shotgun.png");
    ESE::Textures::instance().loadFromFile("GroundBase", "base.png");
    ESE::Textures::instance().loadFromFile("Ground1", "ground1.png");
    ESE::Textures::instance().loadFromFile("Ground2", "ground2.png");
    ESE::Textures::instance().loadFromFile("Ground3", "ground3.png");
    ESE::Textures::instance().loadFromFile("Ground4", "ground4.png");
    ESE::Textures::instance().loadFromFile("Mountain1", "mountain1.png");
    ESE::Textures::instance().loadFromFile("Mountain2", "mountain2.png");
    ESE::Textures::instance().loadFromFile("Mountain3", "mountain3.png");
    ESE::Textures::instance().loadFromFile("Mountain4", "mountain4.png");
    ESE::Textures::instance().loadFromFile("Bird", "bird.png");
    ESE::Textures::instance().loadFromFile("KamikazeBird", "birdbomb.png");
    ESE::Textures::instance().loadFromFile("Carrot","carrot1.png");

    ESE::Fonts::instance().loadFromFile("PixFont", "pixfont.ttf");

    getWindow().create(sf::VideoMode(Config::WIDTH, Config::HEIGHT, 32), "GameTest");
    gameScene = new GameScene("GameScene", &getWindow());
    menuScene = new MenuScene(&getWindow());

    getSceneManager()->addScene(*gameScene);
    getSceneManager()->addScene(*menuScene);
    getSceneManager()->activateSceneAndDeactivateTheRest("MenuScene");
}

MainApplication::~MainApplication() {
    delete menuScene;
    delete gameScene;
}

int main(int argc, char** argv) {
    srand(time(NULL));

    MainApplication app;

    app.run();

    return 0;
}

