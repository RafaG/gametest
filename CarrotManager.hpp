/* 
 * File:   CarrotManager.hpp
 * Author: Rafa
 *
 * Created on 1 de agosto de 2015, 15:37
 */

#ifndef CARROTMANAGER_HPP
#define	CARROTMANAGER_HPP

#include "ActorManager.hpp"
#include <SFML/System/Clock.hpp>

class Carrot;
class Rabbit;

class CarrotManager : public ActorManager<Carrot> {
public:
    CarrotManager(float minY, float maxY);
    virtual ~CarrotManager();
    
    void advanceTime(float deltaTime);
    bool collidesWith(Rabbit & rabbit);
    
    void addCarrot(float minY, float maxY);
    
protected:
    bool isRemovable(Carrot *actor) const;
    
private:
    sf::Clock generationClock;
    float minY, maxY, speed;
};

#endif	/* CARROTMANAGER_HPP */

