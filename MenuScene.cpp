/* 
 * File:   MenuScene.cpp
 * Author: Rafa
 * 
 * Created on 18 de julio de 2015, 1:57
 */

#include "MenuScene.hpp"

#include <ESE/Core/Textures.hpp>
#include <ESE/Core/Fonts.hpp>

MenuScene::MenuScene(sf::RenderWindow * window) : ESE::Scene("MenuScene", window),
rabbit("Rabbit",6,0.1) {
    background.setTexture(*ESE::Textures::instance().getResource("MenuBackground"));
    spiral.setTexture(*ESE::Textures::instance().getResource("Spiral"));
    spiral.setOrigin(spiral.getLocalBounds().width/2,spiral.getLocalBounds().height/2);
    spiral.setPosition(window->getSize().x/2,window->getSize().y);
    
    rabbit.setPosition(sf::Vector2f(rabbit.getSize().x,450));
    rabbit.resume();
    
    options[0] = L"Play";
    options[1] = L"Credits";
    options[2] = L"Exit";
    
    currOption = 0;
    
    option.setFont(*ESE::Fonts::instance().getResource("PixFont"));
    updateOption();
    
}

MenuScene::~MenuScene() {
}

void MenuScene::logic(float deltaTime) {
    spiral.rotate(720*deltaTime);
    
    rabbit.advanceTime(deltaTime);
    rabbit.move(250*deltaTime,0);
    if (rabbit.getPosition().x>800) {
        rabbit.setPosition(sf::Vector2f(rabbit.getSize().x,450));
    }
}

void MenuScene::manageEvents(float deltaTime) {
    
    while (window->pollEvent(events)) {
        this->checkIfWindowClosed();
        
        if (events.type==sf::Event::KeyPressed) {
            if (events.key.code == sf::Keyboard::Left) {
                previousOption();
            }
            if (events.key.code == sf::Keyboard::Right) {
                nextOption();
            }
            if (events.key.code == sf::Keyboard::Return) {
                switch (currOption){
                    case 0:
                        ESE::SceneManager::instance().activateSceneAndDeactivateTheRest("GameScene");
                        break;
                    case 1:
                        break;
                    case 2:
                        ESE::SceneManager::instance().deactivateAllScenes();
                }
            }
        }
    }
    
}

void MenuScene::render() {
    window->clear(sf::Color(100,100,100));
    draw(background);
    draw(spiral);
    draw(option);
    draw(rabbit);
}

void MenuScene::previousOption() {
    if (currOption==0) {
        //currOption = MAX_OPTIONS-1;
    }
    else {
        --currOption;
    }
    
    updateOption();
}

void MenuScene::nextOption() {
    if (currOption==MAX_OPTIONS-1) {
        //currOption = 0;
    }
    else {
        ++currOption;
    }
    
    updateOption();
}

void MenuScene::updateOption() {
    sf::String fText;
    if (currOption==0) {
        fText = options[currOption] + ">";
    }
    else if (currOption==MAX_OPTIONS-1) {
        fText = "<" + options[currOption];
    }
    else {
        fText = "<" + options[currOption] + ">";
    }
    option.setString(fText);
    option.setPosition(window->getSize().x/2-option.getGlobalBounds().width/2,300);
}