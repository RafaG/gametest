/* 
 * File:   Animation.hpp
 * Author: Rafa
 *
 * Created on 24 de junio de 2015, 15:09
 */

#ifndef ANIMATION_HPP
#define	ANIMATION_HPP

#include <ESE/Core/Animatable.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Clock.hpp>

class Animation : public ESE::Animatable, public sf::Transformable, public sf::Drawable {
public:
    Animation(std::string resourceName, int horizontalFrames, float secondsPerFrame = 0);
    virtual ~Animation();
    void draw(sf::RenderTarget & renderTarget, sf::RenderStates states) const;
    void advanceTime(float deltaTime);
    void setTexture(std::string resourceName);
    void nextFrame();
    void setFrame(int frame);
    void pause();
    void resume();
    sf::Vector2f getSize() const;
    void setFramesRange(int start, int end);
    void setFrameTime(float second);
    float getFrameTime() const;
    int getCurrentFrame() const;
    void restartClock();
    void setAlpha(int alpha);
    
    //TRANSFORM
    virtual void scale(float x, float y);
    virtual void setScale(float x, float y);
    virtual void move(float x, float y);
    virtual void move(const sf::Vector2f & mov);
    virtual void setPosition(const sf::Vector2f & pos);
    virtual sf::Vector2f getPosition() const;
    
protected:
    
    sf::Sprite sprite;
    std::vector<sf::IntRect> rects;
    int horizontalFrames;
    int currentFrame;
    float secondsPerFrame;
    sf::Clock clock;
    bool paused;
    int startFrame, endFrame;
};

#endif	/* ANIMATION_HPP */

