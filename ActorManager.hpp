/* 
 * File:   ActorManager.hpp
 * Author: Rafa
 *
 * Created on 4 de agosto de 2015, 14:08
 */

#ifndef ACTORMANAGER_HPP
#define ACTORMANAGER_HPP

#include "Actor.hpp"

template <class T = Actor>
class ActorManager : public Actor {
public:
    ActorManager();
    virtual ~ActorManager();

    void draw(sf::RenderTarget & target, sf::RenderStates states) const;
    void advanceTime(float deltaTime);
    sf::Vector2f getSize() const;
    bool collidesWith(const Actor & actor);
    typename std::vector<T*>::iterator getIteratorActorCollidingWith(const Actor & actor);
    void addActor(T* actor);

    void remove(typename std::vector<T*>::iterator it);
    typename std::vector<T*>::iterator end();


protected:
    virtual bool isRemovable(T *actor) const = 0;

    std::vector<T*> actors;
private:
};

#include "ActorManager.cpp"

#endif /* ACTORMANAGER_HPP */

