/* 
 * File:   Bird.hpp
 * Author: Mario
 *
 * Created on 8 de agosto de 2015, 3:15
 */

#ifndef BIRD_HPP
#define	BIRD_HPP

#include "Animation.hpp"
#include "Enemy.hpp"

class Bird : public Enemy {
public:
    Bird();
    void draw(sf::RenderTarget & target, sf::RenderStates states) const;
    virtual ~Bird();
    void setPosition(sf::Vector2f pos);
    sf::Vector2f getSize() const;
    void advanceTime(float deltaTime);
    bool acceptBulletDamage() const { return true; }

    
private:
    Animation representation;

};

#endif	/* BIRD_HPP */

