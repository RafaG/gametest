/* 
 * File:   CarrotManager.cpp
 * Author: Rafa
 * 
 * Created on 1 de agosto de 2015, 15:37
 */

#include "CarrotManager.hpp"
#include "Rabbit.hpp"
#include "Carrot.hpp"

CarrotManager::CarrotManager(float minY, float maxY) : minY(minY), maxY(maxY) {
    speed = 0.f;
}

CarrotManager::~CarrotManager() {
}

void CarrotManager::advanceTime(float deltaTime) {
    ActorManager<Carrot>::advanceTime(deltaTime);
    
    if (generationClock.getElapsedTime().asSeconds()>15.f) {
        addCarrot(minY,maxY);
        generationClock.restart();
    }
}


bool CarrotManager::collidesWith(Rabbit& rabbit) {
    auto it = this->getIteratorActorCollidingWith(rabbit);
    if (it==end()) {
        return false;
    }
    else {
        remove(it);
        return true;
    }
}

void CarrotManager::addCarrot(float minY, float maxY) {
    Carrot * carrot = new Carrot();
    
    int max = maxY-minY-carrot->getSize().y;
    int randPosY = minY+rand()%max;
    
    carrot->setPosition(800,randPosY);
    carrot->setSpeed(getSpeed());
    
    addActor(carrot);
}

bool CarrotManager::isRemovable(Carrot* actor) const {
    return actor->getPosition().x+actor->getSize().x<0;
}