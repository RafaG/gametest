/* 
 * File:   Carrot.cpp
 * Author: Mario
 * 
 * Created on 28 de julio de 2015, 23:10
 */

#include "Carrot.hpp"

Carrot::Carrot() {
    carrot.loadFromFile("carrot1.png");
    representCarrot.setTexture(carrot);
}

Carrot::~Carrot() {
}

void Carrot::advanceTime(float deltaTime){    
    move(speed*deltaTime);
}

void Carrot::draw(sf::RenderTarget& target, sf::RenderStates states)const{
    states.transform*=this->getTransform();
    target.draw(representCarrot,states); 
}

sf::Vector2f Carrot::getSize() const{
    return sf::Vector2f(representCarrot.getGlobalBounds().width,representCarrot.getGlobalBounds().height);
}

