/* 
 * File:   kamikazeBird.cpp
 * Author: Mario
 * 
 * Created on 19 de junio de 2016, 16:45
 */

#include "kamikazeBird.h"
#include "ESE/Core/Textures.hpp"

KamikazeBird::KamikazeBird(){
    representation.setTexture(*ESE::Textures::instance().getResource("KamikazeBird"));
    representation.setScale(-1,1);
}

KamikazeBird::~KamikazeBird() {
}
void KamikazeBird::draw(sf::RenderTarget & target, sf::RenderStates states) const{
    states.transform*=this->getTransform();
    target.draw(representation,states);
}
sf::Vector2f KamikazeBird::getSize() const {
    return sf::Vector2f(representation.getGlobalBounds().width,representation.getGlobalBounds().height);
}
void KamikazeBird::advanceTime(float deltaTime){
    move(getSpeed()*deltaTime);
    setSpeed(sf::Vector2f(getSpeed().x,getSpeed().y + 50*deltaTime));
    
}

